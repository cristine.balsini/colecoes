import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Box from "../../../components/box-characters";

const RickMortyCharacters = ({ characters, setCharacters }) => {
  const [charactersRick, setCharactersRick] = useState([]);
  const { page } = useParams();

  const removeCard = (x) => {
    let findMatch = characters.some(({ name }) => name === x.name);

    if (findMatch) {
      return;
    }
    setCharacters([...characters, x]);
  };

  const getAPI = () => {
    fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
      .then((resp) => resp.json())
      .then((resp) => {
        const name = resp.results.map((item) => item.name);
        const image = resp.results.map((item) => item.image);
        let rickMorty = [];
        for (let i = 0; i < 20; i++) {
          rickMorty.push({
            name: name[i],
            image: image[i],
            type: "Rick and Morty",
          });
        }
        setCharactersRick(rickMorty);
      });
  };
  useEffect(getAPI, [page]);

  return <Box characterList={charactersRick} selectCard={removeCard} />;
};

export default RickMortyCharacters;
