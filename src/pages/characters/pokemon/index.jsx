import React, { useState, useEffect } from "react";
import Box from "../../../components/box-characters";

const PokemonCharacters = ({
  characters,
  setCharacters,
  offset,
  setOffset,
}) => {
  const [charactersPokemon, setCharactersPokemon] = useState([]);

  const removeCard = (x) => {
    let findMatch = characters.some(({ name }) => name === x.name);

    if (findMatch) {
      return;
    }
    setCharacters([...characters, x]);
  };

  const getAPI = () => {
    fetch(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=20`)
      .then((resp) => resp.json())
      .then((resp) => {
        const result = resp.results.map((item) => {
          const url = item.url.split("/");

          return {
            name: item.name,
            image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
              url[url.length - 2]
            }.png`,
            type: "Pokemon",
          };
        });

        setCharactersPokemon(result);
      });
  };

  useEffect(getAPI, [offset]);

  return <Box characterList={charactersPokemon} selectCard={removeCard} />;
};

export default PokemonCharacters;
