import React, { useEffect } from "react";
import Box from "../../components/box-characters";
import { CharacterDiv, TheButton, CollectionDiv } from "./style.js";

const Collection = ({ characters, setCharacters, switcher, setSwitcher }) => {
  const handleCard = ({ name }) => {
    setCharacters(characters.filter((x) => x.name !== name));
  };

  useEffect(() => {
    window.localStorage.setItem("dados salvos", JSON.stringify(characters));
  }, [characters]);

  const rickCollection = characters.filter(({ name, type, image }) => {
    if (type === "Rick and Morty") {
      return {
        name: name,
        type: type,
        imagem: image,
      };
    }
  });

  const pokeCollection = characters.filter(({ name, type, image }) => {
    if (type === "Pokemon") {
      return {
        name: name,
        type: type,
        imagem: image,
      };
    }
  });

  return (
    <CollectionDiv>
      <CharacterDiv>
        <TheButton
          shape="round"
          size="large"
          onClick={() => {
            switcher && setSwitcher(false);
            !switcher && setSwitcher(true);
          }}
        >
          {switcher ? "Pokemon" : "Rick's"}
        </TheButton>
      </CharacterDiv>
      {switcher === null && (
        <>
          {characters !== undefined && (
            <Box characterList={characters} selectCard={handleCard} />
          )}
        </>
      )}

      {switcher === true && <Box characterList={rickCollection} />}
      {switcher === false && <Box characterList={pokeCollection} />}
    </CollectionDiv>
  );
};

export default Collection;
