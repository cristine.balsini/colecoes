import React from "react";
import { Pie } from "react-chartjs-2";

const Chart = ({ characters, setCharacters }) => {
  const countPoke = characters.filter(({ type }) => {
    if (type === "Pokemon") {
      return {
        type: type,
      };
    }
  });

  const countRick = characters.filter(({ type }) => {
    if (type === "Rick and Morty") {
      return {
        type: type,
      };
    }
  });
  const data = {
    labels: ["Pokemon", "Rick and Morty"],
    datasets: [
      {
        data: [countPoke.length, countRick.length],
        backgroundColor: ["#fEA000", "#FF6B0D"],
        hoverBackgroundColor: ["#FEA500", "#E8810C"],
      },
    ],
  };

  return (
    <>
      <Pie  data={data} />
    </>
  );
};

export default Chart;
