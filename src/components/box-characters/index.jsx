import React from "react";
import { motion } from "framer-motion";
import { DivBox, Cards, Image, SubTitle } from "./style.js";

const Box = ({ characterList, selectCard = () => {} }) => {
  return (
    <DivBox>
      {characterList.map(({ name, type, image }, index) => (
        <motion.div
          whileHover={{ scale: 1.2 }}
          whileTap={{ scale: 0.8 }}
        >
          <Cards
            key={index}
            onClick={() => {
              selectCard({ name, type, image });
            }}
            hoverable
            style={{ margin: 15 }}
            cover={<Image src={image} alt="character" />}
          >
            <SubTitle title={name} />
          </Cards>
        </motion.div>
      ))}
    </DivBox>
  );
};

export default Box;
