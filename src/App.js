import React, { useState } from "react";
import { Switch, Route } from "react-router-dom";
import { Collection, Characters, Chart } from "./pages";
import {
  Header,
  Content,
  Links,
  ChartDiv,
} from "./components/app-style/app-style.js";
import { BiPhotoAlbum, BiMovie } from "react-icons/bi";
import { FaChartPie } from "react-icons/fa";

function App() {
  const [characters, setCharacters] = useState(
    localStorage.getItem("dados salvos")
      ? JSON.parse(window.localStorage.getItem("dados salvos"))
      : []
  );
  const [switcher, setSwitcher] = useState();

  return (
    <>
      <Header>
        <Links onClick={() => setSwitcher(null)} to="/">
          <span>
            <BiPhotoAlbum />
          </span>{" "}
          Collection
        </Links>
        <Links to="/characters/1">
          Characters
          <span>
            {" "}
            <BiMovie />
          </span>
        </Links>
        <Links to="/chart">
          <span>
            {" "}
            <FaChartPie />
          </span>
          Chart
        </Links>
      </Header>

      <Content>
        <Switch>
          <Route exact path="/">
            <Collection
              characters={characters}
              setCharacters={setCharacters}
              switcher={switcher}
              setSwitcher={setSwitcher}
            />
          </Route>
          <Route exact path="/characters/:page">
            <Characters characters={characters} setCharacters={setCharacters} />
          </Route>
        </Switch>
      </Content>

      <Switch>
        <Route exact path="/chart">
          <ChartDiv>
            <Chart characters={characters} setCharacters={setCharacters} />
          </ChartDiv>
        </Route>
      </Switch>
    </>
  );
}

export default App;
